"""
Python script that starts iBeacon on RPi 3
# ref: http://www.theregister.co.uk/Print/2013/11/29/feature_diy_apple_ibeacons/

"""
import os


def start_ibeacon(_uuid="4a 4e ce 60 7e b0 11 e4 b4 a9 08 00 20 0c 9a 66"):
    """
    :param _uuid: UUID to broadcast.
    :return:
    """

    for buf in _uuid.split(' '):
        try:
            tmp = int(buf, 16)
        except ValueError:
            print 'Invalid value for UUID, must be hexadecimal...'
            return False

    # Append '00' to fix the length to 16 byte
    if len(_uuid.split(' ')) < 16:
        while len(_uuid.split(' ')) < 16:
            _uuid += ' 00'
    elif len(_uuid.split(' ')) > 16:
        print 'Error, UUID cannot be longer than 32 bytes.'
        return False
    # OGF = Operation Group Field = Bluetooth Command Group = 0x08
    # OCF = Operation Command Field = HCI_LE_Set_Advertising_Data = 0x0008
    # No. Significant Data Octets (Max of 31) = 1E (Decimal 30)
    # iBeacon Prefix (Always Fixed) = 02 01 1A 1A FF 4C 00 02 15

    # OGF = "0x08"
    # OCF = "0x0008"
    ibeacon_prefix = "1E 02 01 1A 1A FF 4C 00 02 15"

    major = "00 02"
    minor = "00 01"
    power = "C5 00"

    # initialize device
    os.system("sudo hciconfig hci0 up")
    # disable advertising
    os.system("sudo hciconfig hci0 noleadv")

    # stop the dongle looking for other Bluetooth devices
    os.system("sudo hciconfig hci0 noscan")

    os.system("sudo hciconfig hci0 pscan")

    os.system("sudo hciconfig hci0 leadv")

    # advertise
    os.system("sudo hcitool -i hci0 cmd 0x08 0x0008 {} {} {} {} {}".format(ibeacon_prefix, _uuid, major, minor, power))
    os.system("sudo hcitool -i hci0 cmd 0x08 0x0006 A0 00 A0 00 00 00 00 00 00 00 00 00 00 07 00")
    os.system("sudo hcitool -i hci0 cmd 0x08 0x000a 01")

    print 'Completed'


if __name__ == '__main__':

    if os.geteuid() != 0:
        exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")

    start_ibeacon("4a 4e ce 60 7e b0 11 e4 b4 a9 08 00 20 0c 9a 22")
