import json
import platform
import uuid
import re
import time
import ibeacon
from wifi_ctrl import WiFiCtrl
import threading
import os
from flask import Flask, render_template, request
import subprocess


app = Flask(__name__)
cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'
wifi_ctrl = WiFiCtrl()


def broadcast_ip():
    """
    Broadcast current IP address.
    :return:
    """
    while True:
        _ip = WiFiCtrl().get_ip_address('wlan0')
        str_uuid = uuid.uuid4().hex
        if _ip is None:
            str_uuid = str_uuid[:-12] + '0' * 12
        else:
            l_ip = str(_ip).split('.')
            # Add '0' values
            for i in range(len(l_ip)):
                if len(l_ip[i]) < 3:
                    l_ip[i] = '0' * (3-len(l_ip[i])) + l_ip[i]
            str_uuid = str_uuid[:-12] + ''.join(l_ip)

        ibeacon.start_ibeacon(' '.join(re.findall('.{2}', str_uuid)))
        time.sleep(3)


@app.route('/')
def form():
    if platform.system() == 'Linux':
        ap_list = wifi_ctrl.get_ap_list()
    else:
        ap_list = ['aaaaa', 'bbbbb', 'ccccc']
    return render_template('index.html', ap_list=ap_list)


@app.route('/connect_ap', methods=['POST'])
def connect_to_ap():
    _ap = request.form['ap']
    _pwd = request.form['pwd']
    print 'Connecting to {}'.format(_ap)
    os.system('cp ' + cur_dir + 'interfaces /etc/network/interfaces')
    result_ip = wifi_ctrl.connect_to_ap(ssid=_ap, pwd=_pwd)
    if result_ip is not None:
        return json.dumps('true')
    else:
        return json.dumps('false')


def start_ap():
    """
    Start AP mode
    :return:
    """
    os.system('ifdown wlan0')
    os.system('cp ' + cur_dir + 'interfaces_ap /etc/network/interfaces')
    os.system('ifup wlan0')
    os.system('service dnsmasq restart')
    subprocess.Popen(['/usr/sbin/hostapd', '/etc/hostapd/hostapd.conf'])


if __name__ == '__main__':

    if os.geteuid() != 0:
        exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")

    threading.Thread(target=broadcast_ip).start()

    ip = wifi_ctrl.get_ip_address('wlan0')
    if ip is not None:
        if str(ip).split('.')[-1] != '1':
            print 'Already connected to AP'
            print 'Current AP: ', wifi_ctrl.get_current_ap()
            exit(0)

    # Start AP
    start_ap()

    print 'Starting web server...'
    port = 80 if platform.system() == 'Linux' else 8080
    app.run(
        host="0.0.0.0",
        port=port
    )
