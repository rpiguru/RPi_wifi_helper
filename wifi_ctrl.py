import os
import subprocess
import time

import gc
import wifi
import socket
import struct
import fcntl


def is_rpi():
    return os.uname()[4] == 'armv7l'


def is_odroid():
    """
    Check if current system is Odroid or not
    :return:
    """
    return os.uname()[4] == 'aarch64'


class WiFiCtrl:

    def __init__(self):
        pass

    @staticmethod
    def get_ap_list():
        try:
            l_cell = wifi.Cell.all('wlan0')
        except wifi.exceptions.InterfaceError:
            # When wlan0 is down, turn it on and scan again.
            subprocess.call(['/sbin/ifup wlan0'], shell=True)
            l_cell = wifi.Cell.all('wlan0')

        ap_list = []
        for cell in l_cell:
            ap_list.append(cell.ssid)
        return ap_list

    def connect_to_ap(self, ssid, pwd):
        """
        Modify /etc/network/interface file and turn off/on wlan0 interface with ifdown/ifup shell command
        :param ssid: New SSID of ap
        :param pwd: password
        :return:
        """
        # Add quotes at the beginning and the end of values
        # Refer: https://unix.stackexchange.com/questions/102530/escape-characters-in-etc-network-interfaces
        ssid = '"' + ssid + '"'
        pwd = '"' + pwd + '"'

        if is_rpi():
            file_name = '/etc/network/interfaces'
        else:
            file_name = os.path.dirname(os.path.realpath(__file__)) + '/test_net.txt'

        f = open(file_name, 'r+')
        f_content = f.readlines()
        line_num = 0
        for i in range(len(f_content)):
            if {'iface', 'inet', 'wlan0'}.issubset(f_content[i].split()):
                line_num = i
                break
        f_content[line_num] = 'iface wlan0 inet dhcp\n'
        line_num += 1
        if '/etc/wpa_supplicant' in f_content[line_num]:
            line_num += 1
        try:
            f_content[line_num] = '    wpa-ssid ' + ssid + '\n'
        except IndexError:
            f_content.append('    wpa-ssid ' + ssid + '\n')
        line_num += 1
        try:
            f_content[line_num] = '    wpa-psk ' + pwd + '\n'
        except IndexError:
            f_content.append('    wpa-psk ' + pwd + '\n')

        # for i in range(len(f_content)):
        #     if 'wpa-conf /etc/wpa' in f_content[i] and '#' not in f_content[i]:
        #         f_content[i] = "#  " + f_content[i]

        f.seek(0)
        f.truncate()
        f.write(''.join(f_content))
        f.close()

        if is_rpi():
            file_name = '/etc/wpa_supplicant/wpa_supplicant.conf'
            # Remove all network settings in /etc/wpa_supplicant/wpa_supplicant.conf
            f = open(file_name, 'r+')
            f_content = f.readlines()
            line_num = -1
            for i in range(len(f_content)):  # Find the line number that start from `network=`
                if 'network' in f_content[i]:
                    line_num = i
                    break
            if line_num >= 0:
                f_content = f_content[:line_num]
                f.seek(0)
                f.truncate()
                f.write(''.join(f_content))
            f.close()
            proc = subprocess.Popen(['ifdown wlan0'], shell=True, close_fds=True)
            proc.communicate()
            proc.wait()
            time.sleep(1)
            proc = subprocess.Popen(['ifup wlan0'], shell=True, close_fds=True)
            proc.communicate()
            proc.wait()
            del proc
            gc.collect()

            return self.get_ip_address('wlan0')
        else:
            time.sleep(3)
            return self.get_ip_address('eth0')

    @staticmethod
    def get_current_ap():
        """
        Get currently connected AP's SSID
        :return:
        """
        pipe = os.popen('iwgetid -r')
        data = pipe.read().strip()
        pipe.close()
        return data

    @staticmethod
    def get_ip_address(ifname):
        """
        Get assigned IP address of given interface
        :param ifname: interface name such as wlan0, eth0, etc
        :return:
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            ip = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])
            return ip
        except IOError:
            return None


if __name__ == '__main__':

    a = WiFiCtrl()

    print a.get_ip_address('eth0')
