#!/usr/bin/env bash -x

## run this script on host to create AP and setup IP forwarding

# FIX ME: don't assume only one wireless client is connecting to the host access point (AP)

# log start time
echo "Starting: $(date)"

# install dependencies
sudo apt-get update
sudo apt-get -y --fix-missing install nmap

# update dhcpcd.conf
sudo bash -c "grep -qP '^denyinterfaces wlan0$' /etc/dhcpcd.conf || \
    echo -e '\ndenyinterfaces wlan0' >> /etc/dhcpcd.conf"

# update wifi interface settings
sudo sh -c "cat > /etc/network/interfaces << EOF
# interfaces(5) file used by ifup(8) and ifdown(8)

# Please note that this file is written to be used with dhcpcd
# For static IP, consult /etc/dhcpcd.conf and 'man dhcpcd.conf'

# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

allow-hotplug eth1
auto eth1
iface eth1 inet dhcp

allow-hotplug wlan0
auto wlan0
iface wlan0 inet static
    address 172.24.1.1
    netmask 255.255.255.0
    network 172.24.1.0
    broadcast 172.24.1.255

allow-hotplug wlan1
iface wlan1 inet manual
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
EOF"

# create hostapd config file
sudo sh -c "cat > /etc/hostapd/hostapd.conf << EOF
# This is the name of the WiFi interface we configured above
interface=wlan0
# Use the nl80211 driver with the brcmfmac driver
driver=nl80211
# This is the name of the network
ssid=RPi3-AP
# Use the 2.4GHz band
hw_mode=g
# Use channel 6
channel=6
# Enable 802.11n
ieee80211n=1
# Enable WMM
wmm_enabled=1
# Enable 40MHz channels with 20ns guard interval
ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]
# Accept all MAC addresses
macaddr_acl=0
# Use WPA authentication
auth_algs=1
# Require clients to know the network name
ignore_broadcast_ssid=0
# Use WPA2
wpa=2
# Use a pre-shared key
wpa_key_mgmt=WPA-PSK
# The network passphrase
wpa_passphrase=raspberry
# Use AES, instead of TKIP
rsn_pairwise=CCMP
EOF"

## restart wifi
#sudo ifdown wlan0 --force
#sudo ifup wlan0

# update hostapd config
sudo sed -ri 's|^#DAEMON_CONF=.*|DAEMON_CONF="/etc/hostapd/hostapd.conf"|' /etc/default/hostapd

# create new dnsmasq configuration file
[ ! -f /etc/dnsmasq.conf.orig ] && sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
sudo sh -c "cat > /etc/dnsmasq.conf << EOF
interface=wlan0      # Use interface wlan0  
listen-address=172.24.1.1 # Explicitly specify the address to listen on  
bind-interfaces      # Bind to the interface to make sure we aren't sending things elsewhere  
server=8.8.8.8       # Forward DNS requests to Google DNS  
domain-needed        # Don't forward short names  
bogus-priv           # Never forward addresses in the non-routed address spaces.  
dhcp-range=172.24.1.50,172.24.1.150,12h # Assign IP addresses between 172.24.1.50 and 172.24.1.150 with a 12 hour lease time
EOF"

# install iptables-persistent with default options
sudo sh -c 'DEBIAN_FRONTEND=noninteractive apt-get install -y iptables-persistent'

# enable ip forwarding
sudo sed -ri 's/^#net.ipv4.ip_forward=.*/net.ipv4.ip_forward=1/' /etc/sysctl.conf
sudo sh -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'

# configure NAT between wlan0 and ethernet interface
#eth="$(route | grep -P '^default' | grep -Po 'eth\d$')" && echo "Active internet connection on: $eth"
sudo iptables -t filter --flush
sudo iptables -t nat --flush
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
sudo iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE
sudo iptables -A FORWARD -i eth1 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i wlan0 -o eth1 -j ACCEPT
sudo sh -c 'iptables-save > /etc/iptables/rules.v4'

# restore iptables NAT configuration after each boot as last step in rc.local
sudo sed -rni '/^iptables-restore /!p' /etc/rc.local
sudo sed -i '$ i\iptables-restore < /etc/iptables/rules.v4' /etc/rc.local

# reload networking
sudo systemctl daemon-reload
sudo ifdown wlan0
sleep 3
sudo ifup wlan0
sleep 5
